# Clustering-Based Inter-Regional Correlation Estimation
### Authors: Hanâ Lbath, Alexander Petersen, Wendy Meiring, Sophie Achard

This repository contains the code used in the paper titled "Clustering-Based Inter-Regional Correlation Estimation" (arXiv: [https://doi.org/10.48550/arXiv.2302.07596](https://doi.org/10.48550/arXiv.2302.07596)).

The notebook [figure_codes_notebook](./figure_codes_notebook.html) contains codes used to generate the figures and the content of the tables of the paper. The script [simudata_clustcorr_script.R](./simudata_clustcorr_script.R) provides a small example of the correlation estimation on simulated data.
